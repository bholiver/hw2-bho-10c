#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>
#include <vector>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void record_hw1_change(int value);
    void record_hw2_change(int value);
    void record_hw3_change(int value);
    void record_hw4_change(int value);
    void record_hw5_change(int value);
    void record_hw6_change(int value);
    void record_hw7_change(int value);
    void record_hw8_change(int value);

    void record_mt1_change(int value);
    void record_mt2_change(int value);
    void record_final_change(int value);

    void set_schema_A(bool isA);
    void set_schema_B(bool isB);

    void set_class(int);

    void update_score();

private:
    Ui::MainWindow *ui;

    // Vectors to store homework and midterm values
    // [0] = hw1, [1] = hw2... [7] = hw8
    std::vector<int> hwVec10B;
    std::vector<int> hwVec10C;

    // Exam grades don't really need to be stored in a vector
    int mt1_10B, mt1_10C;
    int mt2_10B, mt2_10C;
    int final_10B, final_10C;

    // Character 'A' means schema A, character 'B' means schema B
    char schema;
    // Character 'B' means PIC 10B, character 'C' means PIC 10C
    char theClass;

    // Weighted averages (pre-final number steps)
    double weighted_hw;
    double weighted_midterms;
    double weighted_final;

    // Total grade
    double total_grade;
};

#endif // MAINWINDOW_H
