#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    hwVec10B(std::vector<int>(8, 100)),
    hwVec10C(std::vector<int>(8, 100)),
    mt1_10B(100), mt2_10B(100), final_10B(100),
    mt1_10C(100), mt2_10C(100), final_10C(100),
    theClass('B')
{
    ui->setupUi(this);

    QObject::connect( ui->spinboxHW1, SIGNAL(valueChanged(int)), this, SLOT(record_hw1_change(int)));
    QObject::connect( ui->spinboxHW2, SIGNAL(valueChanged(int)), this, SLOT(record_hw2_change(int)));
    QObject::connect( ui->spinboxHW3, SIGNAL(valueChanged(int)), this, SLOT(record_hw3_change(int)));
    QObject::connect( ui->spinboxHW4, SIGNAL(valueChanged(int)), this, SLOT(record_hw4_change(int)));
    QObject::connect( ui->spinboxHW5, SIGNAL(valueChanged(int)), this, SLOT(record_hw5_change(int)));
    QObject::connect( ui->spinboxHW6, SIGNAL(valueChanged(int)), this, SLOT(record_hw6_change(int)));
    QObject::connect( ui->spinboxHW7, SIGNAL(valueChanged(int)), this, SLOT(record_hw7_change(int)));
    QObject::connect( ui->spinboxHW8, SIGNAL(valueChanged(int)), this, SLOT(record_hw8_change(int)));

    QObject::connect( ui->spinboxMT1, SIGNAL(valueChanged(int)), this, SLOT(record_mt1_change(int)));
    QObject::connect( ui->spinboxMT2, SIGNAL(valueChanged(int)), this, SLOT(record_mt2_change(int)));
    QObject::connect( ui->spinboxFN, SIGNAL(valueChanged(int)), this, SLOT(record_final_change(int)));

    QObject::connect( ui->radio_schemaA, SIGNAL(toggled(bool)), this, SLOT(set_schema_A(bool)));
    QObject::connect( ui->radio_schemaB, SIGNAL(toggled(bool)), this, SLOT(set_schema_B(bool)));

    QObject::connect( ui->comboCourse, SIGNAL(currentIndexChanged(int)), this, SLOT(set_class(int)));
}

void MainWindow::record_hw1_change(int value) {
    (theClass == 'B') ? hwVec10B[0] = value : hwVec10C[0] = value;
    update_score();
    return;
}

void MainWindow::record_hw2_change(int value) {
    (theClass == 'B') ? hwVec10B[1] = value : hwVec10C[1] = value;
    update_score();
    return;
}

void MainWindow::record_hw3_change(int value) {
    (theClass == 'B') ? hwVec10B[2] = value : hwVec10C[2] = value;
    update_score();
    return;
}

void MainWindow::record_hw4_change(int value) {
    (theClass == 'B') ? hwVec10B[3] = value : hwVec10C[4] = value;
    update_score();
    return;
}

void MainWindow::record_hw5_change(int value) {
    (theClass == 'B') ? hwVec10B[4] = value : hwVec10C[4] = value;
    return;
}

void MainWindow::record_hw6_change(int value) {
    (theClass == 'B') ? hwVec10B[5] = value : hwVec10C[5] = value;
    update_score();
    return;
}

void MainWindow::record_hw7_change(int value) {
    (theClass == 'B') ? hwVec10B[6] = value : hwVec10C[6] = value;
    update_score();
    return;
}

void MainWindow::record_hw8_change(int value) {
    (theClass == 'B') ? hwVec10B[7] = value : hwVec10C[7] = value;
    update_score();
    return;
}

void MainWindow::record_mt1_change(int value) {
    (theClass == 'B') ? mt1_10B = value : mt1_10C = value;
    update_score();
    return;
}

void MainWindow::record_mt2_change(int value) {
    (theClass == 'B') ? mt2_10B = value : mt2_10C = value;
    update_score();
    return;
}

void MainWindow::record_final_change(int value) {
    (theClass == 'B') ? final_10B = value : final_10C = value;
    update_score();
    return;
}

void MainWindow::set_schema_A(bool isA) {
    if (isA)
        schema = 'A';
    else
        schema = 'B';

    update_score();
}

void MainWindow::set_schema_B(bool isB) {
    if (isB)
        schema = 'B';
    else
        schema = 'A';

    // Only update the score when a schema is chosen
    update_score();
}

void MainWindow::set_class(int index) {
    if(index == 0) {
        theClass = 'B';
        ui->sliderHW1->setValue(hwVec10B[0]);
        ui->sliderHW2->setValue(hwVec10B[1]);
        ui->sliderHW3->setValue(hwVec10B[2]);
        ui->sliderHW4->setValue(hwVec10B[3]);
        ui->sliderHW5->setValue(hwVec10B[4]);
        ui->sliderHW6->setValue(hwVec10B[5]);
        ui->sliderHW7->setValue(hwVec10B[6]);
        ui->sliderHW8->setValue(hwVec10B[7]);
        ui->spinboxHW1->setValue(hwVec10B[0]);
        ui->spinboxHW2->setValue(hwVec10B[1]);
        ui->spinboxHW3->setValue(hwVec10B[2]);
        ui->spinboxHW4->setValue(hwVec10B[3]);
        ui->spinboxHW5->setValue(hwVec10B[4]);
        ui->spinboxHW6->setValue(hwVec10B[5]);
        ui->spinboxHW7->setValue(hwVec10B[6]);
        ui->spinboxHW8->setValue(hwVec10B[7]);

        ui->sliderMT1->setValue(mt1_10B);
        ui->sliderMT2->setValue(mt2_10B);
        ui->sliderFN->setValue(final_10B);
        ui->spinboxMT1->setValue(mt1_10B);
        ui->spinboxMT2->setValue(mt2_10B);
        ui->spinboxFN->setValue(final_10B);
    }
    else {
        theClass = 'C';
        ui->sliderHW1->setValue(hwVec10C[0]);
        ui->sliderHW2->setValue(hwVec10C[1]);
        ui->sliderHW3->setValue(hwVec10C[2]);
        ui->sliderHW4->setValue(hwVec10C[3]);
        ui->sliderHW5->setValue(hwVec10C[4]);
        ui->sliderHW6->setValue(hwVec10C[5]);
        ui->sliderHW7->setValue(hwVec10C[6]);
        ui->sliderHW8->setValue(hwVec10C[7]);
        ui->spinboxHW1->setValue(hwVec10C[0]);
        ui->spinboxHW2->setValue(hwVec10C[1]);
        ui->spinboxHW3->setValue(hwVec10C[2]);
        ui->spinboxHW4->setValue(hwVec10C[3]);
        ui->spinboxHW5->setValue(hwVec10C[4]);
        ui->spinboxHW6->setValue(hwVec10C[5]);
        ui->spinboxHW7->setValue(hwVec10C[6]);
        ui->spinboxHW8->setValue(hwVec10C[7]);

        ui->sliderMT1->setValue(mt1_10C);
        ui->sliderMT2->setValue(mt2_10C);
        ui->sliderFN->setValue(final_10C);
        ui->spinboxMT1->setValue(mt1_10C);
        ui->spinboxMT2->setValue(mt2_10C);
        ui->spinboxFN->setValue(final_10C);
    }

    update_score();
}

// Function that is called whenever a button is clicked to update the score
void MainWindow::update_score() {

    // If the class is 10B, calculate grades for 10B
    if(theClass == 'B') {
        // Find and lowest homework grade (won't include in sum)
        auto lowest = hwVec10B.begin();
        for (auto iter = hwVec10B.begin(); iter != hwVec10B.end(); ++iter) {
            if (*iter <= *lowest)
                lowest = iter;
        }

        // Declare weights
        double hw_weight;
        double mt1_weight;
        double mt2_weight;
        double final_weight;

        // Set weights accordingly
        // If schema is A (should default to A)
        if (schema != 'B') {
            // Set weights
            hw_weight = 25;
            mt1_weight = 20;
            mt2_weight = 20;
            final_weight = 35;
        }
        // Else, schema is B
        else {
            // Set HW and final_10B weights
            hw_weight = 25;
            final_weight = 45;
            // Set weights for midterms depending on lowest one
            if (mt1_10B <= mt2_10B) {
                mt1_weight = 0;
                mt2_weight = 30;
            }
            else {
                mt1_weight = 30;
                mt2_weight = 0;
            }
        }

        // Calculate weighted HW total
        double hw_sum = 0;
        for (auto item : hwVec10B)
            hw_sum += item;
        // Subtract dropped homework score from the sum
        hw_sum -= *lowest;
        // Divide HW sum by max possible HW points (100 * 7 = 7)
        double max_hw = 100 * 7;
        // Weighted hw = that decimal, * 100, * weight as decimal, so faster just to multiply by weight as percentage
        weighted_hw = (hw_sum / max_hw) * hw_weight;

        // Calculate weighted midterm total
        double max_mt = 100;
        weighted_midterms = ((mt1_10B / max_mt) * mt1_weight) + ((mt2_10B / max_mt) * mt2_weight);

        // Calculate weighted final total
        double max_final = 100;
        weighted_final = ((final_10B / max_final) * final_weight);

        // Calculate total!
        total_grade = weighted_hw + weighted_midterms + weighted_final;

        // Display the grade
        ui->SCORE->setText(QString("PIC 10B: ") + QString::number(total_grade, 'f', 2));
    }
    // Else, calculate grade for 10C
    else {
        // Find and lowest homework grade (won't include in sum)
        auto lowest = hwVec10C.begin();
        for (auto iter = hwVec10C.begin(); iter != hwVec10C.end(); ++iter) {
            if (*iter <= *lowest)
                lowest = iter;
        }

        // Declare weights
        double hw_weight;
        double mt1_weight;
        double mt2_weight;
        double final_weight;

        // Set weights accordingly
        // If schema is A (should default to A)
        if (schema != 'B') {
            // Set weights
            hw_weight = 25;
            mt1_weight = 20;
            mt2_weight = 20;
            final_weight = 35;
        }
        // Else, schema is B
        else {
            // Set HW and final_10B weights
            hw_weight = 25;
            final_weight = 45;
            // Set weights for midterms depending on lowest one
            if (mt1_10C <= mt2_10C) {
                mt1_weight = 0;
                mt2_weight = 30;
            }
            else {
                mt1_weight = 30;
                mt2_weight = 0;
            }
        }

        // Calculate weighted HW total
        double hw_sum = 0;
        for (auto item : hwVec10C)
            hw_sum += item;
        // Subtract dropped homework score from the sum
        hw_sum -= *lowest;
        // Divide HW sum by max possible HW points (100 * 7 = 7)
        double max_hw = 100 * 7;
        // Weighted hw = that decimal, * 100, * weight as decimal, so faster just to multiply by weight as percentage
        weighted_hw = (hw_sum / max_hw) * hw_weight;

        // Calculate weighted midterm total
        double max_mt = 100;
        weighted_midterms = ((mt1_10C / max_mt) * mt1_weight) + ((mt2_10C / max_mt) * mt2_weight);

        // Calculate weighted final total
        double max_final = 100;
        weighted_final = ((final_10C / max_final) * final_weight);

        // Calculate total!
        total_grade = weighted_hw + weighted_midterms + weighted_final;

        // Display the grade
        ui->SCORE->setText(QString("PIC 10C: ") + QString::number(total_grade, 'f', 2));
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}
